package com.itau.jogador.model;

public class Jogador {
	private String nome;
	private int idJogador;
	private int pontos;


	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome(){
		return nome;
	}

	public int getPontos() {
		return pontos;
	}

	public void incrementarPontos(){
		pontos++;
	}
	public void setIdJogador(int idJogador) {
		this.idJogador = idJogador;
	}
	public int getIdJogador() {
		return idJogador;
	}

}