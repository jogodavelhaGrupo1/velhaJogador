package com.itau.jogador.controller;



import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.jogador.model.Jogador;
import com.itau.service.Message;


@Controller
public class JogadorController {

	List<Jogador> jogadores = new ArrayList<Jogador>();
	Message msg = new Message();
	
	@CrossOrigin
	@RequestMapping(path="/jogadores", method=RequestMethod.GET)
	public @ResponseBody
	List<Jogador> getJogador() throws Exception {

		if (jogadores.size() > 0) {
			msg.sendMessage("Jogadores ja existem!");
			return jogadores;
		}
		else {
			Jogador jogador1 = new Jogador();
			jogador1.setNome("Jog1");
			jogador1.setIdJogador(1);
			msg.sendMessage("Criado jogador 1.");

			Jogador jogador2 = new Jogador();
			jogador2.setNome("Jog2");
			jogador2.setIdJogador(2);
			msg.sendMessage("Criado jogador 2.");
			
			jogadores.add(jogador1);
			jogadores.add(jogador2);
		}
		return (jogadores);

	}

	@CrossOrigin
	@RequestMapping(path="/pontos", method=RequestMethod.POST)
	public @ResponseBody
	Jogador pontosJogador(@RequestBody Jogador jogador) throws Exception {
		for (Jogador jog : jogadores) {
			if (jog.getIdJogador() == jogador.getIdJogador()) {
				jog.incrementarPontos();
				msg.sendMessage("Pontos incrementados ao jogador!");
				return jog;
			}
		}
		return null;

	}
}
